# SPDX-FileCopyrightText: 2019 David Edmundson <kde@davidedmundson.co.uk>
# SPDX-License-Identifier: BSD-2-Clause

add_subdirectory(dateandtime)
add_subdirectory(treeview)
add_subdirectory(sounds)
add_subdirectory(mobileform)
